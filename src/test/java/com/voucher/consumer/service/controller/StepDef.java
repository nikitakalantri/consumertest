package com.voucher.consumer.service.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDef {
	
	RestTemplate restTemplate = new RestTemplate();

	HttpHeaders headers = new HttpHeaders();
	HttpStatus expectedRes;

	ConsumerVoucherController consumerVoucherController;
	ResponseEntity<String> response;

	@Given("the client calls get api")
	public void the_client_calls_get_api() {
		consumerVoucherController = new ConsumerVoucherController();
	}

	@When("the client calls \\/api\\/fetchConsumersVouchers")
	public void the_client_calls_api_fetchConsumersVouchers() {
		response = restTemplate.getForEntity("http://localhost:8080" + "/api/fetchConsumersVouchers", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Then("the client receives status code of {int} OK")
	public void the_client_receives_status_code_of_OK(Integer int1) {
		System.out.println("integer value :" + int1);
		System.out.println("responsecodevalue :" + response.getStatusCode());
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@When("the client calls \\/api\\/fetchVouchersForConsumer")
	public void the_client_calls_api_fetchVouchersForConsumer() {
		response = restTemplate.getForEntity("http://localhost:8080" + "/api/fetchVouchersForConsumer", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@When("the client calls \\/api\\/v{int}\\/fetchConsumer")
	public void the_client_calls_api_v_fetchConsumer(Integer int1) {
		response = restTemplate.getForEntity("http://localhost:8080" + "/api/v1/fetchConsumer", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@When("the client calls \\/api\\/v{int}\\/fetchConsumer\\/{int}")
	public void the_client_calls_api_v_fetchConsumer(Integer int1, Integer int2) {
		response = restTemplate.getForEntity("http://localhost:8080" + "/api/v1/fetchConsumer/3", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	/*
	 * @When("the client calls \\/health") public void the_client_calls_health() {
	 * response = restTemplate.getForEntity("http://192.168.0.8:8080" + "/health",
	 * String.class); assertEquals(HttpStatus.OK, response.getStatusCode()); }
	 */

}
