package com.voucher.consumer.service.controller;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", tags = {"@SmokeTest, @CucumberTag"})
public class CucumberHealthCheckTest {

}
