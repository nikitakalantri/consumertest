package com.voucher.consumer.service.controller;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.voucher.consumer.service.Application;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        classes = Application.class
      )
public abstract class SpringIntegrationTest {
	
	protected RestTemplate restTemplate = new RestTemplate();

}
