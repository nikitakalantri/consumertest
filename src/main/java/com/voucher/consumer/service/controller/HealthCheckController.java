package com.voucher.consumer.service.controller;

import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
public class HealthCheckController implements HealthIndicator {

	@Override
	@RequestMapping(value = "/health", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucess"),
			@ApiResponse(code = 204, message = "Empty Contents"),
			@ApiResponse(code = 400, message = "Invalid request data"),
			@ApiResponse(code = 500, message = "Unknown error") })
	public Health health() {
		try {
			URL url = new URL("http://localhost:8080/api/v1/fetchConsumer");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			int code = connection.getResponseCode();
			if (code == 200) {
				return Health.up().withDetail("Success", "Service is UP and Running!").status("200").build();
			} else {
				return Health.down().withDetail("Error", "Service is Down!").build();
			}
		} catch (Exception e) {
			return Health.down().withDetail("Error", "Service is Down!").build();
		}
	}

}
