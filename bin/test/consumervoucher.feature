Feature: Associate consumers and its vouchers

  @CucumberTag
  Scenario: Client makes call to GET /api/fetchConsumersVouchers
    Given the client calls get api
    When the client calls /api/fetchConsumersVouchers
    Then the client receives status code of 200 OK
	@CucumberTag
  Scenario: Client makes call to GET /api/fetchVouchersForConsumer
    Given the client calls get api
    When the client calls /api/fetchVouchersForConsumer
    Then the client receives status code of 200 OK
	@SmokeTest
  Scenario: Client makes call to GET /api/v1/fetchConsumer
    Given the client calls get api
    When the client calls /api/v1/fetchConsumer
    Then the client receives status code of 200 OK
	@CucumberTag
  Scenario: Client makes call to GET /api/v1/fetchConsumer/3
    Given the client calls get api
    When the client calls /api/v1/fetchConsumer/3
    Then the client receives status code of 200 OK
